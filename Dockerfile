FROM node:14.15.4-alpine as prod
ENV NODE_ENV=production
EXPOSE $PORT
RUN mkdir /app && chown -R node:node /app
WORKDIR /app
USER node
COPY --chown=node:node package.json ./
RUN yarn install && yarn cache clean --force
RUN yarn add bcrypt
COPY --chown=node:node . .
CMD [ "yarn", "start" ]

FROM prod as dev
ENV NODE_ENV=development
RUN yarn install --only=development
CMD ["yarn", "start:dev"]
