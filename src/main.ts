import { WINSTON_MODULE_NEST_PROVIDER } from 'nest-winston';
import * as rateLimit from 'express-rate-limit';
import * as helmet from 'helmet';
import { options } from './shared/config/swaggerDocs';
import { NestFactory } from '@nestjs/core';
import {ValidationPipe} from '@nestjs/common'
import {config} from 'dotenv'
import { AppModule } from './app.module';
import { SwaggerModule } from '@nestjs/swagger';
import { NestExpressApplication } from '@nestjs/platform-express';

config();

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app.use(helmet());

  app.enableCors({
    methods: ['GET', 'POST', 'PATCH', 'DELETE']
  })
  
  app.use(
    rateLimit({
      windowMs: 15 * 60 * 1000, // 15 minutes
      max: 100, // limit each IP to 100 requests per windowMs
    }),
  );

  app.set('trust proxy', 1);

  app.setGlobalPrefix('api');
  
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('docs', app, document);
  
  app.useGlobalPipes(new ValidationPipe({
    whitelist: true,
    forbidNonWhitelisted: true,
  }));
   
  app.useLogger(app.get(WINSTON_MODULE_NEST_PROVIDER));

  const port = process.env.PORT || 5000;
  await app.listen(port, ()=> console.log(`listening on port ${port}`));
}
bootstrap();
