import { Logger } from '@nestjs/common';
import { LoggerConfig } from './shared/config/logger';
import { AuthModule } from 'src/modules/auth/auth.module';
import { ResponseInterceptor } from './shared/interceptors/response.interceptor';
import { HttpExceptionFilter } from './shared/filter/exception.filter';
import { Module } from '@nestjs/common';
import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TodoModule } from './modules/todo/todo.module';
import { DatabaseModule } from './database/database.module';
import { UsersModule } from './modules/users/users.module';
import { WinstonModule } from 'nest-winston';
import { LoggerInteceptor } from './shared/interceptors/logger.interceptor';

const logger: LoggerConfig = new LoggerConfig();
@Module({
  imports: [
    WinstonModule.forRoot(logger.console()),
    TodoModule,
    DatabaseModule, 
    UsersModule,
    AuthModule
  ],
  controllers: [AppController],
  providers: [
    Logger,
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: ResponseInterceptor
    },{
      provide: APP_INTERCEPTOR,
      useClass: LoggerInteceptor
    },
    AppService
  ],
})
export class AppModule {}
