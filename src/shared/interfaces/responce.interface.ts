import Todo from "src/modules/todo/todo.entity";
import { User } from "src/modules/users/user.entity";

export interface Response{
    message: string,
    data: Todo | User | any
}