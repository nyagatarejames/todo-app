import { DocumentBuilder } from "@nestjs/swagger";

export const options = new DocumentBuilder()
  .addBearerAuth()
  .setTitle('Todo App')
  .setDescription('The todo app api description')
  .setVersion('1.0.0')
  .build();
