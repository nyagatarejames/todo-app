import winston, { format, transports } from 'winston'

export class LoggerConfig{
    private readonly options: winston.LoggerOptions;

    constructor(){
        this.options = {
            exitOnError: false,
            format: format.combine(format.colorize(), format.timestamp(), format.printf(msg=> {
                return `${msg.timestamp} [${msg.level}] - ${msg.message}`;
            })),
            levels:{
                verbose: 0,
                error: 1,
                warn: 2,
                info: 3,
                debug: 5,
                silly: 6,
            },
            transports: [
                new transports.File({
                    level: 'info',
                    filename: `${__dirname}/../../logs/todoAppinfo.log` 
                }),
                new transports.File({
                    level: 'error',
                    filename: `${__dirname}/../../logs/todoAppErrors.log` 
                }),
                new transports.File({
                    level: 'verbose',
                    filename: `${__dirname}/../../logs/todoAppLogs.log`
                })
            ]
        }
    }

    public console(): object {
        return this.options;
      }
}