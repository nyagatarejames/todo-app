import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {config} from 'dotenv';

config();

const {DB_USER_NAME, DB_PASSWORD, DB_NAME,HOST, NODE_ENV} = process.env;
@Module({
    imports: [TypeOrmModule.forRoot({
        type: 'postgres',
        host: HOST,
        port: 5432,
        username: DB_USER_NAME,
        password: DB_PASSWORD,
        database: DB_NAME,
        entities: [`${__dirname}/**/*.entity{.ts, .js}`],
        synchronize: NODE_ENV !== 'production',
        autoLoadEntities: true,
        logging: NODE_ENV !== 'production',

    })]
})
export class DatabaseModule {}
