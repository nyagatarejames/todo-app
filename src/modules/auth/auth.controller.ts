import { LoginDTO } from 'src/modules/users/dto/login.dto';
import { UserDTO } from 'src/modules/users/dto/user.dto';
import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ApiBadRequestResponse, ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService){}
        
        @ApiCreatedResponse({description: 'Successfully registered'})
        @ApiBadRequestResponse({ description: 'Bad Request'})
        @ApiInternalServerErrorResponse({ description: 'Internal server error' })
        @Post('signup')
        newUser(@Body() user: UserDTO){
            return this.authService.register(user)
        }
        
        @ApiOkResponse({description: 'Successfully logged in'})
        @ApiUnauthorizedResponse({description: 'Something wrong with your token'})
        @ApiBadRequestResponse({ description: 'Bad Request'})
        @ApiInternalServerErrorResponse({ description: 'Internal server error' })
        @Post('login')
        async login(@Body() loginUserDto: LoginDTO) {
            return await this.authService.login(loginUserDto);  
        }
}