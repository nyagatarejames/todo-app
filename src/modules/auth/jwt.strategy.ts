import { Injectable  } from '@nestjs/common';
import { UserDTO } from 'src/modules/users/dto/user.dto';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { AuthService } from './auth.service';
import {config} from 'dotenv'
import { JwtPayload } from './payload.interface';

config();

@Injectable()
/** Class representing jwt strategy to protect some of the routes */
export class JwtStrategy extends PassportStrategy(Strategy) { 
    constructor(private readonly authService: AuthService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('bearer'),
            secretOrKey: process.env.SECRETKEY,
        });
        
    }
    
    /**
     * @description method that validates the token passed by the user
     * @param payload payload transformed from token
     * @returns user
     */
    async validate(payload: JwtPayload): Promise<UserDTO> {
        return await this.authService.validateUser(payload); 
    }
}