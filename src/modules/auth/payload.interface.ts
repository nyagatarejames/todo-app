export interface JwtPayload { 
    id: number, 
    email: string;
    firstname?: string;
    lastName: string;
}