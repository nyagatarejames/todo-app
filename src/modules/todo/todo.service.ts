import { UpdateTodoDTO } from './dto/update-todo.dto';
import {Injectable, NotFoundException } from "@nestjs/common";
import { TodoDTO } from './dto/create-todo.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import  Todo  from './todo.entity';
import { Response } from 'src/shared/interfaces/responce.interface';

@Injectable()
/**Class representing TodoService */
export class TodoService{

    constructor(
        @InjectRepository(Todo)
        private readonly todoRepository: Repository<Todo>
    ){}
    
   /**
    * @description method adding a todo
    * @param todo reqBody from a user
    * @param req id from the payload
    * @returns a message and the created todo
    */
   async addTodo(todo : TodoDTO, req: any): Promise<Response>{
        const created = await this.todoRepository.save( {...todo, userId: req});
        return {
            message: 'You sucessfully Created a todo',
            data: created
        };
    }
    
    /**
     * @description method finding all todos
     * @param userId id from the payload
     * @returns a message and an array of todos
     */
    async findAll(userId: any): Promise<Response>{
        const todos = await this.todoRepository.find({where: {userId}});
        if(!todos.length) throw new NotFoundException('Nothing here just like your crush replies');
        return {
            message:'Successfully retrieved all the todos',
            data: todos
        };
    }

    /**
     * @description method to find a single todo
     * @param id todo id passed in request params 
     * @param userId user id from the payload
     * @returns message and an object of a todo
     */
    async findOne(id: number, userId: number): Promise<Response>{
        const todo = await this.findTodo(id, userId);
        return { 
            message: 'Successfully retrieved a todo',
            data: todo
        }
    }
    
    /**
     * @description method to update a todo
     * @param id the todo id from request params
     * @param userId user id from the payload
     * @param newValue the updates from request body
     * @returns updated todo
     */
    async updateTodo(id: number,userId: number, newValue: UpdateTodoDTO): Promise<Response>{
        await this.findTodo(id, userId);
        await this.todoRepository.update(id, newValue);
        const updated = await this.findTodo(id, userId);
        return {
            message: 'Successfully updated a todo',
            data: updated
        }
    }
    
    /**
     * @description method to delete a todo
     * @param id the todo id
     * @param userId the user id from the payload
     * @returns deleteResult
     */
    async deleteTodo(id: number, userId: number): Promise<DeleteResult> {
        const todo = await this.findTodo(id, userId);
        const result = await this.todoRepository.delete(todo);
        if (!result.affected) throw new NotFoundException('Todo not found');
        return result;
    }

    /**
     * @description method to find a todo
     * @param id the todo id
     * @param userId the User id
     * @returns a todo
     */
    private async findTodo(id: number, userId: number): Promise<Todo>{
        const todo = await this.todoRepository.findOne({where: {id,userId}});
        if (!todo) throw new NotFoundException('Todo not found');
        return todo;
    }
}