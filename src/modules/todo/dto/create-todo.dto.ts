import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsIn, IsOptional, IsString, MinLength } from 'class-validator';
import { priority } from '../types';
export class TodoDTO {
  
    @IsString()
    @MinLength(3, {
      message: 'Title must have atleast 3 characters'
    })
    @ApiProperty()
    public title: string;

    @IsString()
    @MinLength(10, {
      message: 'Description must be atleast 10 characters'
    })
    @ApiProperty()
    public description: string;

    @IsString()
    @IsIn(['HIGH', 'MEDIUM', 'HIGH'], {
      message: 'Priority must be one of HIGH, LOW or MEDIUM'
    })
    @ApiProperty({ 
      enum: ['HIGH', 'MEDIUM', 'LOW'],
      example: 'HIGH'
    })
    public priority: priority;
}
