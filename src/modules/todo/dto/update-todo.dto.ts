import {  ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsIn, IsOptional, IsString, Length, MinLength } from 'class-validator';
import { priority } from '../types';
export class UpdateTodoDTO {

     @IsString()
     @Length(3,15, {
       message: 'Title must have characters between 3 and 15'
     })
     @IsOptional()
     @ApiPropertyOptional({})
     public title: string;

     @IsString()
     @MinLength(10, {
      message: 'Description must be atleast 10 characters'
     })
     @IsOptional()
     @ApiPropertyOptional({example: 'Meeting the new director and discuss on how we can manage the company'})
     public description: string;

     @IsString()
     @IsIn(['HIGH', 'MEDIUM', 'HIGH'], {
      message: 'Priority must be one of HIGH, LOW or MEDIUM'
     })
     @IsOptional()
     @ApiPropertyOptional({ 
      enum: ['HIGH', 'MEDIUM', 'LOW'],
      example: 'HIGH'
    })
     public priority: priority;

     @IsBoolean()
     @IsOptional()
     @ApiPropertyOptional({example: true})
     public isComplete: boolean;
}
  