import { User } from "src/modules/users/user.entity";
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { priority } from './types';

@Entity()
export default class Todo{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false})
    title: string;

    @Column({nullable: false})
    description: string;

    @Column({nullable: false})
    priority: priority;

    @Column({nullable:false, default: false})
    isComplete?: boolean;
    
    @CreateDateColumn({type: 'timestamp', default: 'NOW()'})
    createdAt: Date;

    @UpdateDateColumn({type: 'timestamp', default: 'NOW()'})
    modifiedAt: Date;
     
    @ManyToOne(() => User) userId?: User;

}