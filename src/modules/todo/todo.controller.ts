import { UpdateTodoDTO } from './dto/update-todo.dto';
import { TodoDTO } from './dto/create-todo.dto';
import { TodoService } from './todo.service';
import { Body, Controller, Delete, Get, Param, Patch, Post, Req, UseGuards } from "@nestjs/common";
import { AuthGuard } from '@nestjs/passport';
import { ApiBadRequestResponse, ApiBearerAuth, ApiCreatedResponse, ApiInternalServerErrorResponse, ApiNotFoundResponse, ApiOkResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';

@ApiTags('Todo')
@ApiBearerAuth()
@Controller('todos')
@UseGuards(AuthGuard('jwt'))
export class TodoController {
    constructor(private readonly todoservice: TodoService) {}

    @ApiCreatedResponse({description: 'Successfully created a todo'})
    @ApiUnauthorizedResponse({description: 'Login first'})
    @ApiBadRequestResponse({ description: 'Bad Request'})
    @ApiInternalServerErrorResponse({ description: 'Internal server error' })
    @Post()
    createTodo(@Body() todo: TodoDTO, @Req() req: any) {
        return this.todoservice.addTodo(todo, req.user?.id);
    }

    @ApiOkResponse({description: 'Successfully found all the todos'})
    @ApiUnauthorizedResponse({description: 'Login first'})
    @ApiNotFoundResponse({description: 'Nothing here just like your crush replies'})
    @ApiInternalServerErrorResponse({ description: 'Internal server error' })
    @Get()
    findTodos(@Req() req:any){
        return this.todoservice.findAll(req.user?.id);
    }
 
    @ApiOkResponse({description: 'Successfully found a todo'})
    @ApiUnauthorizedResponse({description: 'Login first'})
    @ApiNotFoundResponse({description: 'No Todo found'})
    @ApiInternalServerErrorResponse({ description: 'Internal server error' })
    @Get(':id')
    findTodo(@Param('id') todoId: number, @Req() req:any ){
        return this.todoservice.findOne(todoId, req.user?.id)
    }

    @ApiOkResponse({description: 'Successfully Updated a todo'})
    @ApiNotFoundResponse({description: 'No Todo found'})
    @ApiUnauthorizedResponse({description: 'Login first'})
    @ApiBadRequestResponse({ description: 'Bad Request'})
    @ApiInternalServerErrorResponse({ description: 'Internal server error' })
    @Patch(':id')
    updateTodo(
        @Param('id') todoId: number,
        @Req() req: any,
        @Body() body: UpdateTodoDTO
    ){
            return this.todoservice.updateTodo(todoId, req.user?.id, body)
    }

    @ApiOkResponse({description: 'Successfully found a todo'})
    @ApiUnauthorizedResponse({description: 'Login first'})
    @ApiNotFoundResponse({description: 'No Todo found'})
    @ApiInternalServerErrorResponse({ description: 'Internal server error' })
    @Delete(':id')
    deleteTodo(@Param('id') todoId: number, @Req() req:any){
        return this.todoservice.deleteTodo(todoId, req.user?.id);
    }

}