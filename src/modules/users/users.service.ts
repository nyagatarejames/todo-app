import { LoginDTO } from './dto/login.dto';
import { UserDTO } from './dto/user.dto';
import { Injectable, ConflictException, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
/**Class representing userServices */
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}
  
  /**
   * @description Finds a user using their email
   * @param userEmail email to be checked against userRepo
   * @returns user object
   */
  public async findByEmail(userEmail: string): Promise<User | null> {
    return await this.userRepository.findOne({ email: userEmail });
  }
  
  /**
   * @description the method that harshes the password
   * @param password the password to be harshed
   * @returns harshed password
   */
  private async hashPassword(password: string) : Promise<string>{
      const harshedPass = await bcrypt.hash(password, 10);
      return harshedPass;
  }

  /**
   * @description the method that registers a user
   * @param createUserDto the request object from the user
   * @returns created user object
   */
  public async register(createUserDto: UserDTO): Promise<User> {
    const { email, password } = createUserDto;
    const user = await this.findByEmail(email);
    if (user) throw new ConflictException('User already exists')
    const hashed = await this.hashPassword(password);
    const newUser = {...createUserDto, password: hashed}
    const created = await this.userRepository.save(newUser);
    delete created.password;
    return created
  }

  /**
   * @description this method logs in a user in the system
   * @param paramLogin email and password from a user
   * @returns user object
   */
  async findByLogin({ email, password }: LoginDTO): Promise<UserDTO> {  
    const user = await this.userRepository.findOne({ where: { email } });
    if (!user) throw new UnauthorizedException();
    const areEqual = await bcrypt.compare(password, user.password);
    
    if (!areEqual) throw new UnauthorizedException();
    delete user.password;
    return user;
}

/**
 * @description finds user when passed a payload
 * @param paramPayLoad provide an email from the user payload
 * @returns user object
 */
async findByPayload({ user: { email } }: any): Promise<UserDTO> {
  const user = await this.userRepository.findOne({ 
      where:  { email } }); 
  delete user.password;
  return user;
}
}
